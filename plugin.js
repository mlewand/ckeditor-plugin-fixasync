/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @fileOverview The "fixasync" plugin.
 *
 */
( function() {
	'use strict';

	CKEDITOR.plugins.add( 'fixasync', {
		requires: 'a11ychecker',

		onLoad: function() {
			var plugin = this;

			CKEDITOR.plugins.a11ychecker.on( 'quickFixesReady', function( repoEvent ) {
				var quickFixRepo = repoEvent.data;

				quickFixRepo.on( 'requested', function( evt ) {
					if ( evt.data.name == 'ChangeTagName' ) {
						CKEDITOR.scriptLoader.load( plugin.path + 'quickfix/ChangeTagName.js', function( res ) {
							if ( res !== true ) {
								// If js file could not be found, mark it as failed.
								evt.data.fail();
							}
						} );

						// Event needs to be canceled, so AC will know that we've already handled this request.
						evt.cancel();
					}
				} );
			} );
		},

		init: function( editor ) {
			// We also need to register the Quick Fix to engine fixes mapping, to "teach"
			// Accessibility Checker that given issue type can be solved with our fix.
			editor._.a11ychecker.on( 'loaded', function() {
				editor._.a11ychecker.engine.fixesMapping.pNotUsedAsHeader = [ 'ChangeTagName' ];
			} );
		},
	} );
} )();
