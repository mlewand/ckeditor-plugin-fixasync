( function() {
	'use strict';

	// This Quick Fix will simply change element tag name.
	function ChangeTagName( issue ) {
		this.issue = issue;
	}

	ChangeTagName.prototype.fix = function( formAttributes, callback ) {
		// Since we can't just change this.issue.element tagName.
		var newElement = new CKEDITOR.dom.element( 'div' );
		this.issue.element.moveChildren( newElement );
		newElement.replace( this.issue.element );

		// We need to call callback, telling that Quick Fix is done. Since this is synchronous
		// fix, we can call it right now.
		if ( callback ) {
			callback( this );
		}
	};

	// This will register our Quick Fix in the repository.
	CKEDITOR.plugins.a11ychecker.quickFixes.add( 'ChangeTagName', ChangeTagName );
}() );